
//déclaration de la variable
let count = 0;

//initialisation de la boucle avec la condition et l'incrémentation
for (let i = 1; i  <= 100; i++) {

    //execution de la boucle avec la 1ere condition
  if (i % 3 === 0 && i % 5 === 0) {
   console.log("FizzBuzz");

//execution de la seconde condition
} else if (i % 3 === 0) {
    console.log("Fizz");

//execution de la troisième condition    
} else if (i % 5 === 0) {
    console.log("Buzz");

} else {
    console.log(i);
}
}

